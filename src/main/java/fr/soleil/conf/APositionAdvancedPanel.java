package fr.soleil.conf;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public abstract class APositionAdvancedPanel extends JPanel implements ActionListener, MouseListener, FocusListener {

    private static final long serialVersionUID = 8240732343606063894L;

    protected ConstrainedCheckBox checkBox;
    protected JLabel checkBoxTitle;

    protected JTextField field1;
    protected JLabel field1Title;
    protected JTextField field2;
    protected JLabel field2Title;

    protected JComboBox<String> combo1;
    protected JLabel combo1Title;
    protected JComboBox<String> combo2;
    protected JLabel combo2Title;
    protected JComboBox<String> combo3;
    protected JLabel combo3Title;

    protected JButton combo3Setter;

    public APositionAdvancedPanel() {
        super();
        initComponents();
    }

    protected void initComponents() {
        setLayout(new GridBagLayout());
        addMouseListener(this);
        initCheckBoxTitle();
        initCheckBox();
        initField1Title();
        initField2Title();
        initField1();
        initField2();
        initCombo1Title();
        initCombo1();
        initCombo2Title();
        initCombo2();
        initCombo3Title();
        initCombo3();
        initCombo3Setter();

        Insets defaultInsets = new Insets(0, 10, 10, 10);
        Insets titleInsets = new Insets(0, 10, 0, 10);

        GridBagConstraints field1TitleConstraints = new GridBagConstraints();
        field1TitleConstraints.fill = GridBagConstraints.HORIZONTAL;
        field1TitleConstraints.gridx = 0;
        field1TitleConstraints.gridy = 0;
        field1TitleConstraints.weightx = 0;
        field1TitleConstraints.weighty = 0;
        field1TitleConstraints.insets = new Insets(0, 10, 10, 0);
        if (field1Title != null) {
            add(field1Title, field1TitleConstraints);
        }
        GridBagConstraints field1Constraints = new GridBagConstraints();
        field1Constraints.fill = GridBagConstraints.HORIZONTAL;
        field1Constraints.gridx = 1;
        field1Constraints.gridy = 0;
        field1Constraints.weightx = 1;
        field1Constraints.weighty = 0;
        field1Constraints.insets = new Insets(0, 0, 10, 10);
        field1Constraints.gridwidth = GridBagConstraints.REMAINDER;
        if (field1 != null) {
            add(field1, field1Constraints);
            field1.addActionListener(this);
            field1.addFocusListener(this);
        }

        GridBagConstraints checkBoxTitleConstraints = new GridBagConstraints();
        checkBoxTitleConstraints.fill = GridBagConstraints.HORIZONTAL;
        checkBoxTitleConstraints.gridx = 0;
        checkBoxTitleConstraints.gridy = 1;
        checkBoxTitleConstraints.weightx = 0.5;
        checkBoxTitleConstraints.weighty = 0;
        checkBoxTitleConstraints.insets = titleInsets;
        if (checkBoxTitle != null) {
            add(checkBoxTitle, checkBoxTitleConstraints);
        }
        GridBagConstraints field2TitleConstraints = new GridBagConstraints();
        field2TitleConstraints.fill = GridBagConstraints.HORIZONTAL;
        field2TitleConstraints.gridx = 1;
        field2TitleConstraints.gridy = 1;
        field2TitleConstraints.weightx = 0.25;
        field2TitleConstraints.weighty = 0;
        field2TitleConstraints.insets = titleInsets;
        if (field2Title != null) {
            add(field2Title, field2TitleConstraints);
        }
        GridBagConstraints combo1TitleConstraints = new GridBagConstraints();
        combo1TitleConstraints.fill = GridBagConstraints.HORIZONTAL;
        combo1TitleConstraints.gridx = 2;
        combo1TitleConstraints.gridy = 1;
        combo1TitleConstraints.weightx = 0.25;
        combo1TitleConstraints.weighty = 0;
        combo1TitleConstraints.insets = titleInsets;
        if (combo1Title != null) {
            add(combo1Title, combo1TitleConstraints);
        }

        GridBagConstraints checkBoxConstraints = new GridBagConstraints();
        checkBoxConstraints.fill = GridBagConstraints.HORIZONTAL;
        checkBoxConstraints.gridx = 0;
        checkBoxConstraints.gridy = 2;
        checkBoxConstraints.weightx = 0.5;
        checkBoxConstraints.weighty = 0;
        checkBoxConstraints.insets = defaultInsets;
        if (checkBox != null) {
            add(checkBox, checkBoxConstraints);
            checkBox.addActionListener(this);
        }
        GridBagConstraints field2Constraints = new GridBagConstraints();
        field2Constraints.fill = GridBagConstraints.HORIZONTAL;
        field2Constraints.gridx = 1;
        field2Constraints.gridy = 2;
        field2Constraints.weightx = 0.25;
        field2Constraints.weighty = 0;
        field2Constraints.insets = defaultInsets;
        if (field2 != null) {
            add(field2, field2Constraints);
            field2.addActionListener(this);
            field2.addFocusListener(this);
        }
        GridBagConstraints combo1Constraints = new GridBagConstraints();
        combo1Constraints.fill = GridBagConstraints.HORIZONTAL;
        combo1Constraints.gridx = 2;
        combo1Constraints.gridy = 2;
        combo1Constraints.weightx = 0.25;
        combo1Constraints.weighty = 0;
        combo1Constraints.insets = defaultInsets;
        if (combo1 != null) {
            add(combo1, combo1Constraints);
            combo1.addActionListener(this);
        }

        GridBagConstraints combo2TitleConstraints = new GridBagConstraints();
        combo2TitleConstraints.fill = GridBagConstraints.HORIZONTAL;
        combo2TitleConstraints.gridx = 0;
        combo2TitleConstraints.gridy = 3;
        combo2TitleConstraints.weightx = 0.5;
        combo2TitleConstraints.weighty = 0;
        combo2TitleConstraints.insets = titleInsets;
        if (combo2Title != null) {
            add(combo2Title, combo2TitleConstraints);
        }
        GridBagConstraints combo3TitleConstraints = new GridBagConstraints();
        combo3TitleConstraints.fill = GridBagConstraints.HORIZONTAL;
        combo3TitleConstraints.gridx = 1;
        combo3TitleConstraints.gridy = 3;
        combo3TitleConstraints.weightx = 0.5;
        combo3TitleConstraints.weighty = 0;
        combo3TitleConstraints.gridwidth = 2;
        combo3TitleConstraints.insets = titleInsets;
        if (combo3Title != null) {
            add(combo3Title, combo3TitleConstraints);
        }

        GridBagConstraints combo2Constraints = new GridBagConstraints();
        combo2Constraints.fill = GridBagConstraints.HORIZONTAL;
        combo2Constraints.gridx = 0;
        combo2Constraints.gridy = 4;
        combo2Constraints.weightx = 0.5;
        combo2Constraints.weighty = 0;
        combo2Constraints.insets = defaultInsets;
        if (combo2 != null) {
            add(combo2, combo2Constraints);
            combo2.addActionListener(this);
        }
        GridBagConstraints combo3Constraints = new GridBagConstraints();
        combo3Constraints.fill = GridBagConstraints.HORIZONTAL;
        combo3Constraints.gridx = 1;
        combo3Constraints.gridy = 4;
        combo3Constraints.weightx = 0.5;
        combo3Constraints.weighty = 0;
        combo3Constraints.gridwidth = 2;
        combo3Constraints.insets = defaultInsets;
        if (combo3 != null) {
            add(combo3, combo3Constraints);
            combo3.addActionListener(this);
        }
        GridBagConstraints combo3SetterConstraints = new GridBagConstraints();
        combo3SetterConstraints.fill = GridBagConstraints.BOTH;
        combo3SetterConstraints.gridx = 3;
        combo3SetterConstraints.gridy = 4;
        combo3SetterConstraints.weightx = 0;
        combo3SetterConstraints.weighty = 0;
        combo3SetterConstraints.gridwidth = 2;
        combo3SetterConstraints.insets = new Insets(0, 0, 10, 0);
        if (combo3Setter != null) {
            add(combo3Setter, combo3SetterConstraints);
            combo3Setter.addActionListener(this);
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == checkBox) {
            checkBoxValidated();
        } else if (e.getSource() == field1) {
            field1Validated();
        } else if (e.getSource() == field2) {
            field2Validated();
        } else if (e.getSource() == combo1) {
            combo1Validated();
        } else if (e.getSource() == combo2) {
            combo2Validated();
        } else if (e.getSource() == combo3) {
            combo3Validated();
        } else if (e.getSource() == combo3Setter) {
            combo3SetterValidated();
        }
    }

    protected abstract void initCheckBoxTitle();

    protected abstract void initCheckBox();

    protected abstract void initField1Title();

    protected abstract void initField1();

    protected abstract void initField2Title();

    protected abstract void initField2();

    protected abstract void initCombo1Title();

    protected abstract void initCombo1();

    protected abstract void initCombo2Title();

    protected abstract void initCombo2();

    protected abstract void initCombo3Title();

    protected abstract void initCombo3();

    protected abstract void initCombo3Setter();

    protected abstract void checkBoxValidated();

    protected abstract void field1Validated();

    protected abstract void field2Validated();

    protected abstract void combo1Validated();

    protected abstract void combo2Validated();

    protected abstract void combo3Validated();

    protected abstract void combo3SetterValidated();

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        grabFocus();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (e.getSource() == field1) {
            field1Validated();
        } else if (e.getSource() == field2) {
            field2Validated();
        }
    }

    public static void main(String[] args) {
        APositionAdvancedPanel panel = new APositionAdvancedPanel() {

            private static final long serialVersionUID = -5556047125238305019L;

            @Override
            protected void checkBoxValidated() {
            }

            @Override
            protected void combo1Validated() {
            }

            @Override
            protected void combo2Validated() {
            }

            @Override
            protected void combo3SetterValidated() {
            }

            @Override
            protected void combo3Validated() {
            }

            @Override
            protected void field1Validated() {
            }

            @Override
            protected void field2Validated() {
            }

            @Override
            protected void initCheckBox() {
                checkBox = new ConstrainedCheckBox();
            }

            @Override
            protected void initCheckBoxTitle() {
                checkBoxTitle = new JLabel("checkBox", SwingConstants.LEFT);
            }

            @Override
            protected void initCombo1() {
                combo1 = new JComboBox<>();
                combo1.addItem("Val1");
                combo1.addItem("Val2");
            }

            @Override
            protected void initCombo1Title() {
                combo1Title = new JLabel("combo1", SwingConstants.CENTER);
            }

            @Override
            protected void initCombo2() {
                combo2 = new JComboBox<>();
                combo2.addItem("Val1");
                combo2.addItem("Val2");
            }

            @Override
            protected void initCombo2Title() {
                combo2Title = new JLabel("combo2", SwingConstants.CENTER);
            }

            @Override
            protected void initCombo3() {
                combo3 = new JComboBox<>();
                combo3.addItem("Val1");
                combo3.addItem("Val2");
            }

            @Override
            protected void initCombo3Setter() {
                combo3Setter = new JButton("set");
                combo3Setter.setMargin(new Insets(0, 0, 0, 0));
            }

            @Override
            protected void initCombo3Title() {
                combo3Title = new JLabel("combo3", SwingConstants.CENTER);
            }

            @Override
            protected void initField1() {
                field1 = new JTextField("0");
            }

            @Override
            protected void initField1Title() {
                field1Title = new JLabel("field 1", SwingConstants.RIGHT);
            }

            @Override
            protected void initField2() {
                field2 = new JTextField("0");
            }

            @Override
            protected void initField2Title() {
                field2Title = new JLabel("field2", SwingConstants.CENTER);
            }
        };
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setContentPane(panel);
        mainFrame.setSize(400, 300);
        mainFrame.setVisible(true);
    }

}
