package fr.soleil;

public class Encoder extends Counter {

    protected String decoding = null;
    protected boolean zIndex; // true for "enabled"
    protected String zIndexPhase = null;
    protected int zIndexVal;
    protected String mode = null;
    protected String units = null;
    protected String value = null;
    protected String proxy = null;

    public Encoder(String name) {
        super(name);
    }

    public String getDecoding() {
        if (decoding == null)
            return "";
        return decoding;
    }

    public void setDecoding(String decoding) {
        this.decoding = decoding;
    }

    public boolean isZIndex() {
        return zIndex;
    }

    public void setZIndex(boolean index) {
        zIndex = index;
    }

    public String getZIndexPhase() {
        if (zIndexPhase == null)
            return "";
        return zIndexPhase;
    }

    public void setZIndexPhase(String indexPhase) {
        zIndexPhase = indexPhase;
    }

    public int getZIndexVal() {
        return zIndexVal;
    }

    public void setZIndexVal(int indexVal) {
        zIndexVal = indexVal;
    }

    public String getMode() {
        if (mode == null)
            return "";
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUnits() {
        if (units == null)
            return "";
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getValue() {
        if (value == null)
            return "";
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProxy() {
        if (proxy == null)
            return "";
        return proxy;
    }

    public void setProxy(String proxy) {
        this.proxy = proxy;
    }

    @Override
    public StringBuilder toStringBuilder() {
        StringBuilder buffer = super.toStringBuilder();
        buffer.append("\nMode:").append("POS");
        buffer.append("\nDecoding:").append(getDecoding());
        buffer.append("\nZindex:").append(isZIndex() ? "Enabled" : "Disabled");
        buffer.append("\nZindexPhase:").append(getZIndexPhase());
        buffer.append("\nZindexVal:").append(Integer.toString(getZIndexVal()));
        buffer.append("\nEncoderMode:").append(getMode());
        buffer.append("\nUnits:").append(getUnits());
        if ("linear".equalsIgnoreCase(getMode())) {
            buffer.append("\nDistancePerPulse:").append(getValue());
        } else {
            buffer.append("\nPulsePerRev:").append(getValue());
        }
        buffer.append("\nProxy:").append(getProxy());
        return buffer;
    }

    @Override
    public void clean() {
        super.clean();
        decoding = null;
        zIndexPhase = null;
        mode = null;
        units = null;
        value = null;
        proxy = null;
    }

}
