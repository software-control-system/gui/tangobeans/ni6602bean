package fr.soleil;

public abstract class Counter {

    protected String name = null;

    public Counter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public StringBuilder toStringBuilder() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("Name:").append(name);
        return buffer;
    }

    @Override
    public String toString() {
        return toStringBuilder().toString();
    }

    public void clean() {
        name = null;
    }

}
