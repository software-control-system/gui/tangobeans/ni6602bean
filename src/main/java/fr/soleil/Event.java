package fr.soleil;

public class Event extends Counter {

    protected String edgeType = null;
    protected String direction = null;

    public Event(String name) {
        super(name);
    }

    public String getEdgeType() {
        if (edgeType == null)
            return "";
        return edgeType;
    }

    public void setEdgeType(String edgeType) {
        this.edgeType = edgeType;
    }

    public String getDirection() {
        if (direction == null)
            return "";
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public StringBuilder toStringBuilder() {
        StringBuilder buffer = super.toStringBuilder();
        buffer.append("\nMode:").append("EVT");
        buffer.append("\nEdgeType:").append(getEdgeType());
        buffer.append("\nDirection:").append(getDirection());
        return buffer;
    }

    @Override
    public void clean() {
        super.clean();
        edgeType = null;
        direction = null;
    }

}
