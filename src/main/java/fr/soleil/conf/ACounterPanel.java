package fr.soleil.conf;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.soleil.NI6602;

public abstract class ACounterPanel extends JPanel implements ActionListener, MouseListener {

    private static final long serialVersionUID = -7365700188471170716L;

    protected JLabel combo1Title;
    protected JComboBox<String> combo1;
    protected JLabel combo2Title;
    protected JComboBox<String> combo2;
    protected JLabel pictureLabel;

    public ACounterPanel() {
        super();
    }

    protected void initComponents() {
        setLayout(new GridBagLayout());
        addMouseListener(this);
        initPictureLabel();
        initCombo1Title();
        initCombo2Title();
        initCombo1();
        initCombo2();

        Insets defaultInsets = new Insets(0, 10, 10, 10);
        Insets titleInsets = new Insets(10, 10, 0, 10);

        GridBagConstraints combo1TitleConstraints = new GridBagConstraints();
        combo1TitleConstraints.fill = GridBagConstraints.BOTH;
        combo1TitleConstraints.gridx = 0;
        combo1TitleConstraints.gridy = 0;
        combo1TitleConstraints.weightx = 1;
        combo1TitleConstraints.weighty = 0;
        combo1TitleConstraints.insets = titleInsets;
        if (combo1Title != null) {
            add(combo1Title, combo1TitleConstraints);
        }

        GridBagConstraints combo1Constraints = new GridBagConstraints();
        combo1Constraints.fill = GridBagConstraints.BOTH;
        combo1Constraints.gridx = 0;
        combo1Constraints.gridy = 1;
        combo1Constraints.weightx = 1;
        combo1Constraints.weighty = 0;
        combo1Constraints.insets = defaultInsets;
        if (combo1 != null) {
            add(combo1, combo1Constraints);
            combo1.addActionListener(this);
        }

        GridBagConstraints pictureLabelConstraints = new GridBagConstraints();
        pictureLabelConstraints.fill = GridBagConstraints.BOTH;
        pictureLabelConstraints.gridx = 2;
        pictureLabelConstraints.gridy = 1;
        pictureLabelConstraints.weightx = 0;
        pictureLabelConstraints.insets = defaultInsets;
        pictureLabelConstraints.gridheight = GridBagConstraints.REMAINDER;
        if (pictureLabel != null) {
            add(pictureLabel, pictureLabelConstraints);
        }

        GridBagConstraints combo2TitleConstraints = new GridBagConstraints();
        combo2TitleConstraints.fill = GridBagConstraints.BOTH;
        combo2TitleConstraints.gridx = 0;
        combo2TitleConstraints.gridy = 3;
        combo2TitleConstraints.weightx = 1;
        combo2TitleConstraints.weighty = 1;
        combo2TitleConstraints.insets = titleInsets;
        if (combo2Title != null) {
            add(combo2Title, combo2TitleConstraints);
        }
        GridBagConstraints combo2Constraints = new GridBagConstraints();
        combo2Constraints.fill = GridBagConstraints.BOTH;
        combo2Constraints.gridx = 0;
        combo2Constraints.gridy = 4;
        combo2Constraints.weightx = 1;
        combo2Constraints.weighty = 0;
        combo2Constraints.insets = defaultInsets;
        if (combo2 != null) {
            add(combo2, combo2Constraints);
            combo2.addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == combo1) {
            combo1Validated();
        } else if (e.getSource() == combo2) {
            combo2Validated();
        }
    }

    protected void initPictureLabel() {
        pictureLabel = new JLabel(new ImageIcon(NI6602.class.getResource("activeUp.gif")));
        pictureLabel.setVerticalAlignment(SwingConstants.TOP);
    }

    protected abstract void initCombo1Title();

    protected abstract void initCombo1();

    protected abstract void initCombo2Title();

    protected abstract void initCombo2();

    protected abstract void combo1Validated();

    protected abstract void combo2Validated();

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        grabFocus();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    public static void main(String[] args) {
        ACounterPanel panel = new ACounterPanel() {

            private static final long serialVersionUID = 3534757399866702896L;

            @Override
            protected void combo1Validated() {
                if (combo1 != null && pictureLabel != null && combo1.getSelectedIndex() > -1) {
                    if (combo1.getSelectedIndex() == 0) {
                        pictureLabel.setIcon(new ImageIcon(NI6602.class.getResource("activeUp.gif")));
                    } else {
                        pictureLabel.setIcon(new ImageIcon(NI6602.class.getResource("activeDown.gif")));
                    }
                }
            }

            @Override
            protected void combo2Validated() {
            }

            @Override
            protected void initCombo1() {
                combo1 = new JComboBox<>();
                combo1.addItem("front montant");
                combo1.addItem("front descendant");
            }

            @Override
            protected void initCombo1Title() {
                combo1Title = new JLabel("combo1");
            }

            @Override
            protected void initCombo2() {
                combo2 = new JComboBox<>();
                combo2.addItem("val1");
                combo2.addItem("val2");
            }

            @Override
            protected void initCombo2Title() {
                combo2Title = new JLabel("combo2");
                combo2Title.setVerticalAlignment(SwingConstants.BOTTOM);
            }
        };
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setContentPane(panel);
        mainFrame.setSize(400, 200);
        mainFrame.setVisible(true);
    }

}
