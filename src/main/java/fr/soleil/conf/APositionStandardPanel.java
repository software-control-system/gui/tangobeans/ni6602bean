package fr.soleil.conf;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public abstract class APositionStandardPanel extends JPanel implements ActionListener, MouseListener, FocusListener {

    private static final long serialVersionUID = -5866273435568592472L;

    protected JLabel label1;
    protected JLabel label2;
    protected JTextField field1;
    protected JComboBox<String> field2;

    public APositionStandardPanel() {
        super();
    }

    protected void initCompoments() {
        setLayout(new GridBagLayout());
        addMouseListener(this);
        initLabel1();
        initLabel2();
        initField1();
        initField2();

        GridBagConstraints label1Constraints = new GridBagConstraints();
        label1Constraints.fill = GridBagConstraints.HORIZONTAL;
        label1Constraints.gridx = 0;
        label1Constraints.gridy = 0;
        label1Constraints.weightx = 0.5;
        label1Constraints.weighty = 0;
        label1Constraints.insets = new Insets(0, 0, 0, 10);
        if (label1 != null) {
            add(label1, label1Constraints);
        }
        GridBagConstraints label2Constraints = new GridBagConstraints();
        label2Constraints.fill = GridBagConstraints.HORIZONTAL;
        label2Constraints.gridx = 1;
        label2Constraints.gridy = 0;
        label2Constraints.weightx = 0.5;
        label2Constraints.weighty = 0;
        label2Constraints.insets = new Insets(0, 0, 0, 10);
        if (label2 != null) {
            add(label2, label2Constraints);
        }

        GridBagConstraints field1Constraints = new GridBagConstraints();
        field1Constraints.fill = GridBagConstraints.BOTH;
        field1Constraints.gridx = 0;
        field1Constraints.gridy = 1;
        field1Constraints.weightx = 0.5;
        field1Constraints.weighty = 1;
        field1Constraints.insets = new Insets(0, 0, 0, 10);
        if (field1 != null) {
            add(field1, field1Constraints);
            field1.addActionListener(this);
            field1.addFocusListener(this);
        }
        GridBagConstraints field2Constraints = new GridBagConstraints();
        field2Constraints.fill = GridBagConstraints.BOTH;
        field2Constraints.gridx = 1;
        field2Constraints.gridy = 1;
        field2Constraints.weightx = 0.5;
        field2Constraints.weighty = 1;
        field2Constraints.insets = new Insets(0, 0, 0, 10);
        if (field2 != null) {
            add(field2, field2Constraints);
            field2.addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == field1) {
            field1Validated();
        } else if (e.getSource() == field2) {
            field2Validated();
        }
    }

    protected abstract void initLabel1();

    protected abstract void initLabel2();

    protected abstract void initField1();

    protected abstract void initField2();

    protected abstract void field1Validated();

    protected abstract void field2Validated();

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        grabFocus();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (e.getSource() == field1) {
            field1Validated();
        }
    }
}
