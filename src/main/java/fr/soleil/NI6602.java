package fr.soleil;

import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.target.ITarget;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

public class NI6602 extends AbstractTangoBox implements ActionListener {

    private static final long serialVersionUID = 6416539293129646017L;

    protected static final String SCALAR_IDENTIFIER = "Scalars";
    protected static final String SPECTRUM_IDENTIFIER = "Spectrums";
    protected static final String NO_DATA_IDENTIFIER = "Not connected";

    protected final Chart allIn1Viewer;
    protected final List<Chart> spectrumViewers;
    protected final List<Label> scalarViewers;
    protected final List<JPanel> scalarViewPanels;
    protected NI6602Model ni6602Model;
    protected final JPanel scalarPanel;
    protected final JPanel spectrumPanel;
    protected final JPanel spectrumViewPanel;
    protected final ConstrainedCheckBox allIn1CheckBox;
    protected final JLabel noConnectionLabel;
    protected final ChartViewerBox chartBox;
    protected final CardLayout cardLayout;

    public NI6602() {
        super();

        cardLayout = new CardLayout();
        setLayout(cardLayout);

        allIn1Viewer = new Chart();
        spectrumViewers = new ArrayList<>();
        scalarViewers = new ArrayList<>();
        scalarViewPanels = new ArrayList<>();
        scalarPanel = new JPanel();
        spectrumViewPanel = new JPanel();
        spectrumPanel = new JPanel();
        allIn1CheckBox = new ConstrainedCheckBox("All spectrums in 1 viewer");
        allIn1CheckBox.setSelected(true);
        allIn1CheckBox.addActionListener(this);
        spectrumPanel.setLayout(new GridBagLayout());
        noConnectionLabel = new JLabel("Not connected", JLabel.CENTER);
        noConnectionLabel.setVerticalAlignment(JLabel.CENTER);
        chartBox = new ChartViewerBox();

        add(noConnectionLabel, NO_DATA_IDENTIFIER);
        add(scalarPanel, SCALAR_IDENTIFIER);
        add(spectrumPanel, SPECTRUM_IDENTIFIER);

        GridBagConstraints checkBoxConstraints = new GridBagConstraints();
        checkBoxConstraints.fill = GridBagConstraints.BOTH;
        checkBoxConstraints.gridx = 0;
        checkBoxConstraints.gridy = 0;
        checkBoxConstraints.weightx = 1;
        checkBoxConstraints.weighty = 0;
        spectrumPanel.add(allIn1CheckBox, checkBoxConstraints);
        GridBagConstraints panelConstraints = new GridBagConstraints();
        panelConstraints.fill = GridBagConstraints.BOTH;
        panelConstraints.gridx = 0;
        panelConstraints.gridy = 1;
        panelConstraints.weightx = 1;
        panelConstraints.weighty = 1;
        spectrumPanel.add(spectrumViewPanel, panelConstraints);

        updateLayoutForViewers();
    }

    @Override
    protected void clearGUI() {
        cardLayout.show(this, NO_DATA_IDENTIFIER);
        for (Iterator<ITarget> targetIterator = connectedWidgetMap.keySet().iterator(); targetIterator.hasNext();) {
            cleanWidget(targetIterator.next());

        }
        for (int i = 0; i < scalarViewPanels.size(); i++) {
            scalarViewPanels.get(i).removeAll();
        }
        spectrumViewers.clear();
        scalarViewers.clear();
        scalarViewPanels.clear();
        scalarPanel.removeAll();
        spectrumViewPanel.removeAll();
        ni6602Model.clean();
        ni6602Model = null;
    }

    @Override
    protected void onConnectionError() {
        System.err.println("Failed to connect to NI6602 device");
    }

    @Override
    protected void refreshGUI() {
        if (model != null) {
            ni6602Model = new NI6602Model(model);
        }
        updateLayoutForViewers();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == allIn1CheckBox) {
            updateLayoutForSpectrumViewers();
        }
    }

    protected void updateLayoutForViewers() {
        cardLayout.show(this, NO_DATA_IDENTIFIER);
        if (ni6602Model != null) {
            if (ni6602Model.isBuffered()) {
                updateLayoutForSpectrumViewers();
                cardLayout.show(this, SPECTRUM_IDENTIFIER);
            } else {
                updateLayoutForScalarViewers();
                cardLayout.show(this, SCALAR_IDENTIFIER);
            }
        }
    }

    protected void updateLayoutForSpectrumViewers() {
        for (int i = 0; i < scalarViewers.size(); i++) {
            cleanWidget(scalarViewers.get(i));
            scalarViewPanels.get(i).removeAll();
        }
        scalarViewers.clear();
        scalarViewPanels.clear();
        scalarPanel.removeAll();
        spectrumViewPanel.removeAll();
        StringBuilder errorBuffer = new StringBuilder();
        TangoKey attributeKey;
        if (allIn1CheckBox.isSelected()) {
            spectrumViewPanel.setLayout(new GridLayout());
            cleanWidget(allIn1Viewer);
            spectrumViewPanel.add(allIn1Viewer);
            if (ni6602Model != null) {
                for (int i = 0; i < ni6602Model.getCounters().size(); i++) {
                    String counterName = ni6602Model.getCounters().get(i).getName();
                    attributeKey = generateAttributeKey(counterName);
                    setWidgetModel(allIn1Viewer, chartBox, attributeKey);
                    // try to keep the same color
                    if (spectrumViewers.size() > i) {
                        // clean (should not clean PlotProperties)
                        cleanWidget(spectrumViewers.get(i));
                    }
                }
            }
        } else {
            if (ni6602Model == null) {
                spectrumViewers.clear();
            } else {
                if (spectrumViewers.size() < ni6602Model.getCounters().size()) {
                    while (spectrumViewers.size() < ni6602Model.getCounters().size()) {
                        spectrumViewers.add(new Chart());
                    }
                } else {
                    while (spectrumViewers.size() > ni6602Model.getCounters().size()) {
                        cleanWidget(spectrumViewers.get(spectrumViewers.size() - 1));
                        spectrumViewers.remove(spectrumViewers.size() - 1);
                    }
                }
                int columns = 0;
                int rows = 0;
                while (columns * columns < spectrumViewers.size()) {
                    columns++;
                }
                while (columns * rows < spectrumViewers.size()) {
                    rows++;
                }
                GridLayout layout = new GridLayout(rows, columns);
                for (int i = 0; i < spectrumViewers.size(); i++) {
                    spectrumViewPanel.add(spectrumViewers.get(i));
                }
                spectrumViewPanel.setLayout(layout);
                for (int i = 0; i < ni6602Model.getCounters().size(); i++) {
                    String counterName = ni6602Model.getCounters().get(i).getName();
                    attributeKey = generateAttributeKey(counterName);
                    setWidgetModel(spectrumViewers.get(i), chartBox, attributeKey);
                }
            }

            // clean (should not clean PlotProperties)
            cleanWidget(allIn1Viewer);
        }
        if (errorBuffer.length() > 0) {
            JOptionPane.showMessageDialog(this, errorBuffer.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        errorBuffer = null;
        spectrumViewPanel.revalidate();
        spectrumViewPanel.repaint();
        repaint();
    }

    protected void updateLayoutForScalarViewers() {
        for (Chart spectrumViewer : spectrumViewers) {
            cleanWidget(spectrumViewer);
        }
        spectrumViewers.clear();
        spectrumViewPanel.removeAll();
        GridLayout layout = new GridLayout();
        scalarPanel.removeAll();
        if (ni6602Model == null) {
            for (int i = 0; i < scalarViewers.size(); i++) {
                cleanWidget(scalarViewers.get(i));
                scalarViewPanels.get(i).removeAll();
            }
            scalarViewers.clear();
            scalarViewPanels.clear();
        } else {
            if (scalarViewers.size() < ni6602Model.getCounters().size()) {
                while (scalarViewers.size() < ni6602Model.getCounters().size()) {
                    Label viewer = generateLabel();
                    JPanel panel = new JPanel();
                    panel.setLayout(new GridLayout());
                    panel.add(viewer);
                    scalarViewers.add(viewer);
                    scalarViewPanels.add(panel);
                    panel = null;
                    viewer = null;
                }
            } else {
                while (scalarViewers.size() > ni6602Model.getCounters().size()) {
                    cleanWidget(scalarViewers.get(scalarViewers.size() - 1));
                    scalarViewPanels.get(scalarViewers.size() - 1).removeAll();
                    scalarViewPanels.remove(scalarViewers.size() - 1);
                    scalarViewers.remove(scalarViewers.size() - 1);
                }
            }
            int n = 0;
            int m = 0;
            while (n * n < scalarViewers.size()) {
                n++;
            }
            while (n * m < scalarViewers.size()) {
                m++;
            }
            layout.setColumns(n);
            layout.setRows(m);
            for (int i = 0; i < scalarViewPanels.size(); i++) {
                scalarPanel.add(scalarViewPanels.get(i));
            }
            scalarPanel.setLayout(layout);

            TangoKey attributeKey;
            for (int i = 0; i < ni6602Model.getCounters().size(); i++) {
                String counterName = ni6602Model.getCounters().get(i).getName();
                attributeKey = generateReadOnlyAttributeKey(counterName);
                setWidgetModel(scalarViewers.get(i), stringBox, attributeKey);

                String scalarName = ni6602Model.getDeviceName() + "/" + counterName;
                scalarViewPanels.get(i).setBorder(new TitledBorder(scalarName));
            }
        }

        scalarPanel.revalidate();
        scalarPanel.repaint();
        repaint();
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    public static void main(String[] args) throws Exception {
        NI6602 bean = new NI6602();
        bean.setModel(args[0]);
        bean.start();
        JFrame frame = new JFrame("NI6602 Bean on " + args[0]);
        frame.getContentPane().add(bean);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setSize(800, 600);
    }

}
