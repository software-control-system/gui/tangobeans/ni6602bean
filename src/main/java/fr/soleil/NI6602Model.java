package fr.soleil;


import java.util.ArrayList;
import java.util.Arrays;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;

public class NI6602Model {

    protected String deviceName = null;
    protected ArrayList<String>  boards   = null;
    protected boolean            isMaster;
    protected boolean            buffered;
    protected ArrayList<String>  slaves   = null;
    protected ArrayList<Counter> counters = null;

    public NI6602Model(String deviceName) {
        this.deviceName = deviceName;
        boards = new ArrayList<String>();
        isMaster = false;
        slaves = new ArrayList<String>();
        counters = new ArrayList<Counter>();
        readProperty();
    }

    public void readProperty() {
        try {
            DeviceProxy device = new DeviceProxy(deviceName);
            String[] properties = device.get_property_list("*");
            boards.clear();
            slaves.clear();
            counters.clear();
            for (int i = 0; i < properties.length; i++) {
                DbDatum property = device.get_property( properties[i] );
                if ( property != null ) {
                    if ( "Boards".equalsIgnoreCase( property.name ) ) {
                        String[] boardsValue = property.extractStringArray();
                        boards.addAll( Arrays.asList(boardsValue) );
                    }
                    else if ( "isMaster".equalsIgnoreCase(property.name) ) {
                        isMaster = property.extractBoolean();
                    }
                    else if ( "Buffered".equalsIgnoreCase(property.name) ) {
                        buffered = property.extractBoolean();
                    }
                    else if ( "Slaves".equalsIgnoreCase(property.name) ) {
                        String[] slavesValue = property.extractStringArray();
                        slaves.addAll( Arrays.asList(slavesValue) );
                    }
                    else if ( property.name.toLowerCase()
                            .startsWith("counter") ) {
                        String[] counterDetails = property.extractStringArray();
                        String name = null;
                        String mode = null;
                        Counter counter;
                        for (int j = 0; j < counterDetails.length; j++) {
                            if ( counterDetails[j].toLowerCase().startsWith(
                                    "name" ) ) {
                                name = counterDetails[j].split(":")[1];
                                if (mode != null) break;
                            }
                            else if ( counterDetails[j].toLowerCase()
                                    .startsWith("mode") ) {
                                mode = counterDetails[j].split(":")[1];
                                if (name != null) break;
                            }
                        }
                        if ( "evt".equalsIgnoreCase(mode) ) {
                            counter = new Event(name);
                            for (int j = 0; j < counterDetails.length; j++) {
                                if ( counterDetails[j].toLowerCase()
                                        .startsWith("edgetype") ) {
                                    ( (Event) counter )
                                            .setEdgeType( counterDetails[j]
                                                    .split(":")[1] );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith("direction") ) {
                                    ( (Event) counter )
                                            .setDirection( counterDetails[j]
                                                    .split(":")[1] );
                                }
                            }
                        }
                        else {
                            counter = new Encoder(name);
                            for (int j = 0; j < counterDetails.length; j++) {
                                if ( counterDetails[j].toLowerCase()
                                        .startsWith("decoding") ) {
                                    ( (Encoder) counter )
                                            .setDecoding( counterDetails[j]
                                                    .split(":")[1] );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith("zindexval") ) {
                                    ( (Encoder) counter ).setZIndexVal( Integer
                                            .parseInt( counterDetails[j]
                                                    .split(":")[1] ) );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith("zindexphase") ) {
                                    ( (Encoder) counter )
                                            .setZIndexPhase( counterDetails[j]
                                                    .split(":")[1] );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith( "zindex" ) ) {
                                    ( (Encoder) counter )
                                            .setZIndex( "enabled"
                                                    .equalsIgnoreCase( counterDetails[j]
                                                            .split(":")[1] ) );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith( "encodermode" ) ) {
                                    ( (Encoder) counter )
                                            .setMode( counterDetails[j]
                                                    .split(":")[1] );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith("units") ) {
                                    ( (Encoder) counter )
                                            .setUnits( counterDetails[j]
                                                    .split(":")[1] );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith("distanceperpulse") ) {
                                    ( (Encoder) counter )
                                            .setValue( counterDetails[j]
                                                    .split(":")[1] );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith("pulseperrev") ) {
                                    ( (Encoder) counter )
                                            .setValue( counterDetails[j]
                                                    .split(":")[1] );
                                }
                                else if ( counterDetails[j].toLowerCase()
                                        .startsWith("proxy") ) {
                                    ( (Encoder) counter )
                                            .setProxy( counterDetails[j]
                                                    .split(":")[1] );
                                }
                            } // end for (int j = 0; j < counterDetails.length; j++)
                        } // end if ( "evt".equalsIgnoreCase(mode) ) ... else
                        if ( counter != null ) {
                            counters.add(counter);
                        }
                    } // end else if ( property.name.toLowerCase().startsWith("counter") )
                } // end if ( property != null )
            } // end for (int i = 0; i < properties.length; i++)
        }
        catch (DevFailed e) {
            e.printStackTrace();
        }
    }

    public void writeProperty() {
        try {
            DeviceProxy device = new DeviceProxy(deviceName);
            String[] propertyNames = device.get_property_list("*");
            // Clean properties first.
            for (int i = 0; i < propertyNames.length; i++) {
                device.delete_property( propertyNames[i] );
            }
            // Then write properties
            DbDatum property = new DbDatum("Boards");
            property.insert( getBoards().toArray( new String[0] ) );
            device.put_property(property);
            property = new DbDatum("isMaster");
            property.insert( isMaster() );
            device.put_property(property);
            property = new DbDatum("Slaves");
            property.insert( getSlaves().toArray( new String[0] ) );
            device.put_property(property);
            for (int i = 0; i < getCounters().size(); i++) {
                property = new DbDatum( "counter" + (i+1) );
                property.insert( getCounters().get(i).toString().split("\n") );
                device.put_property(property);
            }
        }
        catch (DevFailed e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getBoards () {
        return boards;
    }

    public boolean isMaster () {
        return isMaster;
    }

    public boolean isBuffered () {
        return buffered;
    }

    public ArrayList<String> getSlaves () {
        return slaves;
    }

    public ArrayList<Counter> getCounters () {
        return counters;
    }

    public String getDeviceName () {
        return deviceName;
    }

    /**
     * Cleans references before removing this reference.
     */
    public void clean() {
        boards.clear();
        slaves.clear();
        for (int i = 0; i < counters.size(); i++) {
            counters.get(i).clean();
        }
        counters.clear();
        deviceName = null;
    }

}
