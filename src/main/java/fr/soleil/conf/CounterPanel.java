package fr.soleil.conf;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.soleil.Event;
import fr.soleil.NI6602;

public class CounterPanel extends ACounterPanel {

    private static final long serialVersionUID = -4293861027696078188L;

    protected Event model;

    protected final static ImageIcon RAISING = new ImageIcon(NI6602.class.getResource("activeUp.gif"));
    protected final static ImageIcon FALLING = new ImageIcon(NI6602.class.getResource("activeDown.gif"));

    public CounterPanel(Event model) {
        super();
        this.model = model;
        initComponents();
    }

    @Override
    protected void combo1Validated() {
        if (model != null) {
            model.setEdgeType(combo1.getSelectedItem().toString());
        }
        if (combo1.getSelectedIndex() == 0) {
            pictureLabel.setIcon(new ImageIcon(NI6602.class.getResource("activeUp.gif")));
        } else {
            pictureLabel.setIcon(new ImageIcon(NI6602.class.getResource("activeDown.gif")));
        }
    }

    @Override
    protected void combo2Validated() {
        if (model != null) {
            model.setDirection(combo2.getSelectedItem().toString());
        }
    }

    @Override
    protected void initCombo1() {
        combo1 = new JComboBox<>();
        combo1.addItem("Rising");
        combo1.addItem("Falling");
        if (model != null) {
            if ("Rising".equalsIgnoreCase(model.getEdgeType())) {
                combo1.setSelectedIndex(0);
            } else if ("Falling".equalsIgnoreCase(model.getEdgeType())) {
                combo1.setSelectedIndex(1);
            } else
                combo1Validated();
        }
    }

    @Override
    protected void initCombo1Title() {
        combo1Title = new JLabel("Edge Type");
        combo1Title.setHorizontalAlignment(SwingConstants.CENTER);
        combo1Title.setVerticalAlignment(SwingConstants.BOTTOM);
    }

    @Override
    protected void initCombo2() {
        combo2 = new JComboBox<>();
        combo2.addItem("UP");
        combo2.addItem("DOWN");
        if (model != null) {
            if ("UP".equalsIgnoreCase(model.getDirection())) {
                combo2.setSelectedIndex(0);
            } else if ("DOWN".equalsIgnoreCase(model.getDirection())) {
                combo2.setSelectedIndex(1);
            } else
                combo2Validated();
        }
    }

    @Override
    protected void initCombo2Title() {
        combo2Title = new JLabel("Direction");
        combo2Title.setHorizontalAlignment(SwingConstants.CENTER);
        combo2Title.setVerticalAlignment(SwingConstants.BOTTOM);
    }

    @Override
    protected void initPictureLabel() {
        pictureLabel = new JLabel(RAISING);
        pictureLabel.setVerticalAlignment(SwingConstants.TOP);
    }

    public static void main(String[] args) {
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setContentPane(new CounterPanel(null));
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

}
